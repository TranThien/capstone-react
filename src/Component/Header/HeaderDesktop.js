import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { setUserLogOut } from "../../redux/action/userAction";
import { userLocalStorage } from "../../service/localStorageService";

export default function HeaderDesktop() {
  const user = useSelector((state) => {
    return state.userReducer.user;
  });
  let dispatch = useDispatch();

  const handleRenderUserLogin = () => {
    // nếu user đăng nhập thành công
    if (user) {
      return (
        <>
          <button className="self-center px-8 py-3 hover:text-red-500 border-solid divide-violet-400 border-r flex items-center">
            <img
              alt=""
              className="w-8 h-8 mr-4 rounded-full ring-2 ring-offset-4 dark:bg-gray-500 ring-violet-400 ring-offset-gray-800"
              src="https://source.unsplash.com/40x40/?portrait?1"
            />
            {user?.hoTen}
          </button>
          <NavLink className="" to="/login">
            <button
              onClick={handleLogout}
              className="self-center px-8 py-3 rounded hover:text-red-500 "
            >
              Đăng xuất
            </button>
          </NavLink>
        </>
      );
    } else {
      // Nếu user đăng nhập thất bại
      return (
        <>
          <NavLink to="/register">
            <button className="self-center px-8 py-3 hover:text-red-500 border-solid divide-violet-400 border-r">
              Đăng Ký
            </button>
          </NavLink>
          <NavLink className="" to="/login">
            <button className="self-center px-8 py-3 rounded hover:text-red-500 ">
              Đăng Nhập
            </button>
          </NavLink>
        </>
      );
    }
  };

  // const handleLogOut = (e) => {
  //   e.preventDefault();

  //   // chuyển hướng về trang đăng nhập
  //   window.location.href = "/login";

  //   userLocalStorage.remove();
  // };

  const handleLogout = () => {
    dispatch(setUserLogOut(null));
  };

  return (
    <header className="p-4 bg-gray-800 text-gray-100 fixed w-full top-0 z-50">
      <div className="container flex justify-between h-16 mx-auto">
        <NavLink
          rel="noopener noreferrer"
          to="/"
          aria-label="Back to homepage"
          className="flex items-center p-2"
        >
          <img
            src="https://www.cgv.vn/skin/frontend/cgv/default/images/cgvlogo.png"
            alt=""
          />
        </NavLink>
        <ul className="items-stretch space-x-3 flex">
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#lichchieu"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 focus:border-violet-400"
            >
              Lịch Chiếu
            </a>
          </li>
          <li className="flex">
            <NavLink
              rel="noopener noreferrer"
              to="#"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 focus:border-violet-400"
            >
              Cụm Rạp
            </NavLink>
          </li>
          <li className="flex">
            <NavLink
              rel="noopener noreferrer"
              to="#"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 focus:border-violet-400"
            >
              Tin Tức
            </NavLink>
          </li>
          <li className="flex">
            <NavLink
              rel="noopener noreferrer"
              to="#"
              className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 focus:border-violet-400"
            >
              Ứng Dụng
            </NavLink>
          </li>
        </ul>
        <div className="items-center flex-shrink-0 flex">
          {handleRenderUserLogin()}
        </div>
      </div>
    </header>
  );
}
