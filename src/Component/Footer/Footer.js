import React from "react";

export default function Footer() {
  return (
    <footer className="px-4 divide-y bg-gray-800 text-gray-100">
      <div className="container flex flex-col justify-center py-10 mx-auto space-y-8 lg:flex-row lg:space-y-0">
        <div className="grid grid-cols-2 text-sm gap-x-3 gap-y-8 lg:w-2/3 sm:grid-cols-4">
          <div className="space-y-3">
            <h3 className="tracking-wide uppercase dark:text-gray-50"> CGV</h3>
            <ul className="space-y-1">
              <li>
                <a rel="noopener noreferrer" href="#">
                  Thỏa thuận sử dụng
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  Brand Guidelines
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  Chính sách bảo mật
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  FAQ
                </a>
              </li>
            </ul>
          </div>
          <div className="space-y-3 ">
            <h3 className="tracking-wide uppercase dark:text-gray-50">
              Đối tác
            </h3>
            <ul className="space-y-1 grid grid-cols-4 gap-3 flex flex-col justify-center items-center">
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download.jpg"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (1).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (1).jpg"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (2).jpg"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (2).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (3).jpg"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (3).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (4).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (11).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download.jpg"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (6).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (6).png"
                    style={{
                      width: 30,
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                    alt=""
                  />
                </a>
              </li>
            </ul>
          </div>
          <div className="space-y-3">
            <h3 className="uppercase dark:text-gray-50">MOBILE APP</h3>
            <ul className="space-y-1 flex">
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img
                    src="./img/download (9).png"
                    style={{ width: 50 }}
                    className="mr-3"
                    alt=""
                  />
                </a>
              </li>
              <li>
                <a rel="noopener noreferrer" href="#">
                  <img src="./img/download.png" style={{ width: 50 }} alt="" />
                </a>
              </li>
            </ul>
          </div>
          <div className="space-y-3">
            <div className="uppercase dark:text-gray-50">SOCIAL</div>
            <div className="flex justify-start space-x-3">
              <a
                rel="noopener noreferrer"
                href="#"
                title="Facebook"
                className="flex items-center p-1"
              >
                <img
                  src="./img/download (10).png"
                  style={{ width: 50 }}
                  alt=""
                />
              </a>

              <a
                rel="noopener noreferrer"
                href="#"
                title="Instagram"
                className="flex items-center p-1"
              >
                <img
                  src="./img/download (7).png"
                  style={{ width: 50 }}
                  alt=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="py-6 text-sm text-center dark:text-gray-400">
        <h1>TIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h1>© 1968 Company Co. All
        rights reserved.
      </div>
    </footer>
  );
}
