import React from "react";
import { Button, message } from "antd";
import { Checkbox, Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import style from "./loginPage.module.css";
import Lottie from "lottie-react";
import bg_cute_buny from "../../asset/72700-cute-bunnies-love-animation.json";
import { setUserActionService } from "../../redux/action/userAction";
export default function LoginPage() {
  // dùng chuyển hướng trang
  const navigate = useNavigate();
  // dùng đá dữ liệu lên reduxx
  const dispatch = useDispatch();
  // redux thunk
  const onFinish = (values) => {
    let navigation = () => {
      setTimeout(() => {
        //chuyển hướng về trang chủ
        navigate("/");
      }, 1000);
    };
    dispatch(setUserActionService(values, navigation));
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className={style.bgLogin}>
      <div className="container bg-white mx-auto px-5 py-10 flex h-1/4 w-2/3 rounded">
        <div className="w-1/2">
          <Lottie animationData={bg_cute_buny} />
        </div>
        <div className="w-1/2">
          <h3 className="text-center text-2xl text-amber-600 mb-10">
            Đăng Nhập
          </h3>
          <Form
            name="basic"
            labelCol={{
              span: 6,
            }}
            wrapperCol={{
              span: 16,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 6,
                span: 16,
              }}
            >
              <Button
                className="bg-cyan-400 outline-offset-0 text-red-500"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
          <div className="text-right mr-8 hover:text-red-600 text-green-400 underline underline-offset-1">
            <NavLink to="/register">Bạn chưa có tài khoản ? Đăng ký</NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}
