import React from "react";
import Lottie from "lottie-react";
import bg_foundPage from "../../asset/36395-lonely-404.json";
import { NavLink, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft, faHouse } from "@fortawesome/free-solid-svg-icons";

export default function NotFoundPage() {
  const style = {
    paddingTop: "96px",
  };
  const navigate = useNavigate();
  return (
    <div style={style}>
      <div
        className="text-center text-red-500
      "
      >
        <h1>
          The page you are looking for might have been removed, had its name
          changed or is temporarily unavailable.
          <p>Please try to</p>
          <div className="mt-5">
            <a
              className="bg-green-400 mr-3 px-3 px-5 text-zinc-600 inline-block"
              onClick={() => navigate(-1)}
            >
              <FontAwesomeIcon className="mr-2" icon={faArrowLeft} />
              Go back
            </a>
            <NavLink
              className="bg-green-400 px-3 px-5  text-zinc-600 inline-block"
              to="/"
            >
              <FontAwesomeIcon className="mr-2" icon={faHouse} />
              Return to the homepage
            </NavLink>
          </div>
        </h1>
      </div>
      <Lottie animationData={bg_foundPage} />
    </div>
  );
}
