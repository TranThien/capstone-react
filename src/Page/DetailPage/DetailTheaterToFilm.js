import React, { useEffect, useState } from "react";
import { movieService } from "../../service/movieService";
import { Tabs } from "antd";
import moment from "moment";
import { NavLink } from "react-router-dom";

export default function DetailTheaterToFilm(props) {
  const id = props.detail.id;
  const [detailTheaterFollowFilm, setDetailTheaterFilm] = useState(null);
  useEffect(() => {
    movieService
      .getDetailInforFilmAndTheater(id)
      .then((res) => {
        setDetailTheaterFilm(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);
  const onChange = (key) => {
    console.log(key);
  };

  const renderScheduleSystemTheaterToFilm = () => {
    return detailTheaterFollowFilm?.heThongRapChieu.map((item, index) => {
      return {
        label: <img src={item.logo} style={{ width: 60 }} alt="" />,
        key: index,
        children: <div>{renderScheduleTheaterFilm(item)}</div>,
      };
    });
  };
  const renderScheduleTheaterFilm = (item) => {
    return item.cumRapChieu.map((cumRap, index) => {
      return (
        <div key={index}>
          <h1 className="text-lime-500 text-xl">{cumRap.tenCumRap}</h1>
          <div className="grid grid-cols-4 gap-3">
            {cumRap.lichChieuPhim.slice(0, 8).map((item, index) => {
              return (
                <NavLink
                  key={index}
                  to="/buyticket/:id"
                  className="bg-slate-300 px-2 py-3 mt-3 w-42 text-center text-rose-600 hover:text-blue-600"
                >
                  {moment(item.ngayChieuGioChieu).format("LLL")}
                </NavLink>
              );
            })}
          </div>
        </div>
      );
    });
  };
  return (
    <div className="py-5 container px-6 mx-auto">
      <Tabs
        id="muave"
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderScheduleSystemTheaterToFilm()}
      />
    </div>
  );
}
