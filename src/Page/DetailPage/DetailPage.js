import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieService } from "../../service/movieService";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import { Button, Modal } from "antd";
import DetailTheaterToFilm from "./DetailTheaterToFilm";

export default function DetailPage() {
  const detail = useParams();
  const [detailFilm, setDetailFilm] = useState(null);
  // lấy thông tin film chi tiết
  useEffect(() => {
    movieService.getDetailMovie(detail.id).then((res) => {
      setDetailFilm(res.data.content);
    });
  }, [detail.id]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  // const handleOk = () => {
  //   setIsModalOpen(false);
  // };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const renderDetailFilm = () => {
    return (
      <div className="flex justify-between px-6 items-center">
        <div className="rounded" style={{ width: 210, height: 320 }}>
          <img
            src={detailFilm?.hinhAnh}
            className="object-contain w-full block"
            alt=""
          />
        </div>
        <div className="text-left flex-1 ml-6 text-red-600">
          <span className="text-xl">
            {moment(detailFilm?.ngayKhoiChieu).format("LLL")}
          </span>
          <h2 className="text-3xl py-5">{detailFilm?.tenPhim}</h2>
          <h3 className="text-xl">Thời lượng : 120 phút</h3>
          <div className="pt-5">
            <a href="#muave">
              <button className="bg-red-500 text-white px-5 py-3 rounded mr-2">
                Mua vé
              </button>
            </a>
            <button
              onClick={showModal}
              className="bg-red-500 text-white px-5 py-3 rounded mr-2"
            >
              Xem Trailer
            </button>
            <Modal
              width={600}
              className="bg-zinc-500"
              bodyStyle={{ height: 400 }}
              title="Trailer"
              open={isModalOpen}
              // // onOk={handleOk}
              onCancel={handleCancel}
              footer={null}
            >
              <div
                className="embed-responsive embed-responsive-21by9 relative w-full h-full overflow-hidden"
                style={{ paddingTop: "42.857143%" }}
              >
                <iframe
                  className="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                  src={detailFilm?.trailer}
                  allowFullScreen
                  data-gtm-yt-inspected-2340190_699="true"
                  id={240632615}
                />
              </div>
            </Modal>
          </div>
        </div>

        <div>
          <div className="w-32 h-32 rounded-full bg-rose-600 flex items-center justify-center ">
            <span className="text-4xl text-center text-slate-100">10</span>
          </div>
          <div className="text-2xl">
            <FontAwesomeIcon className="text-amber-400" icon={faStar} />
            <FontAwesomeIcon className="text-amber-400" icon={faStar} />
            <FontAwesomeIcon className="text-amber-400" icon={faStar} />
            <FontAwesomeIcon className="text-amber-400" icon={faStar} />
            <FontAwesomeIcon className="text-amber-400" icon={faStar} />
          </div>
        </div>
      </div>
    );
  };
  return (
    <div className="pt-24">
      <div className="container mx-auto py-12">{renderDetailFilm()}</div>
      <div>
        <DetailTheaterToFilm detail={detail} />
      </div>
    </div>
  );
}
