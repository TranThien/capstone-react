import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { movieService } from "../../../service/movieService";

export default function Banner() {
  const contentStyle = {
    height: "600px",
    width: "100%",
    color: "#fff",
    lineHeight: "600px",
    textAlign: "center",
  };
  const [banner, setBanner] = useState([]);

  useEffect(() => {
    movieService.getBanner().then((res) => {
      setBanner(res.data.content);
    });
  }, []);
  const renderBanner = () => {
    return banner.map((item, index) => {
      return (
        <div key={index}>
          <img
            src={item.hinhAnh}
            className="w-full object-cover"
            style={contentStyle}
            alt="hinh anh"
          />
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderBanner()}</Carousel>;
}
