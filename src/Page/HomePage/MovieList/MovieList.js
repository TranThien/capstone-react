import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { movieService } from "../../../service/movieService";
import { NavLink } from "react-router-dom";

const { Meta } = Card;

export default function MovieList() {
  const [listMovie, setListMovie] = useState([]);
  useEffect(() => {
    movieService.getListMovie().then((res) => {
      setListMovie(res.data.content);
    });
  }, []);

  const renderCard = () => {
    return listMovie?.slice(0, 15).map((item, index) => {
      return (
        <Card
          className="shadow-2xl"
          key={index}
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" className="h-60 " src={item.hinhAnh} />}
        >
          <Meta
            title={
              <h2 className="text-center text-green-500">{item.tenPhim}</h2>
            }
            description={
              <p className="h-16 text-justify">
                {item.moTa.length > 60
                  ? item.moTa.slice(0, 60) + "..."
                  : item.moTa}
              </p>
            }
          />
          <div className="text-center mt-5 bg-red-500 rounded py-3 hover:text-red-600 hover:bg-orange-300">
            <NavLink className="block" to={`/detail/${item.maPhim}`}>
              Xem chi tiết
            </NavLink>
          </div>
        </Card>
      );
    });
  };
  return (
    <div className="flex items-center justify-center">
      <div className="grid md:grid-cols-3 gap-4 lg:grid-cols-5">
        {renderCard()}
      </div>
    </div>
  );
}
