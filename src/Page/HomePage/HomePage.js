import React from "react";
import Banner from "./Banner/Banner";
import MovieList from "./MovieList/MovieList";
import TabTheater from "./TabTheater/TabTheater";

export default function HomePage() {
  return (
    <div className="pt-24">
      <Banner />
      <div className="pt-20 w-full">
        <MovieList />
      </div>
      <TabTheater />
    </div>
  );
}
