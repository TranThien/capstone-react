import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../../service/movieService";
import moment from "moment/moment";
import { NavLink } from "react-router-dom";

const onChange = (key) => {};

export default function TabTheater() {
  const [theater, setTheater] = useState([]);
  useEffect(() => {
    movieService.getInforSystemTheater().then((res) => {
      setTheater(res.data.content);
    });
  }, []);
  useEffect(() => {}, []);
  // render rạp theo từng hệ thống
  const renderTheaterFollowSystem = (item) => {
    return item.lstCumRap.map((theater, index) => {
      return {
        label: <div>{theater.tenCumRap}</div>,
        key: index,
        children: (
          <div style={{ height: 700, overflowY: "scroll" }}>
            {renderDetailTheater(theater)}
          </div>
        ),
      };
    });
  };
  // render từng rạp chi tiết
  const renderDetailTheater = (theater) => {
    return theater.danhSachPhim.map((item, index) => {
      return (
        <div key={index} className="p-3 flex">
          <div>
            <img
              src={item.hinhAnh}
              style={{ width: 86, height: 126, objectFit: "cover" }}
              alt=""
            />
          </div>
          <div className="ml-4">
            <div className="flex items-center">
              <span className="bg-red-500 text-zinc-50 px-3 py-1 ">C18</span>
              <h2 className="text-2xl text-orange-400 hover:text-red-600 ml-2">
                {item.tenPhim}
              </h2>
            </div>
            <div className="grid grid-cols-3 gap-5 mt-5">
              {item.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
                return (
                  <NavLink to="/buyticket/:id" key={index}>
                    <div className="bg-red-500 text-zinc-50 px-2 py-1">
                      {moment(item.ngayChieuGioChieu).format("LLL")}
                    </div>
                  </NavLink>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  };
  // thông tin thông tin hệ thống rạp
  const renderInforTheater = () => {
    return theater.map((item, index) => {
      return {
        label: <img src={item.logo} style={{ width: 50 }} alt="" />,
        key: index,
        children: (
          <Tabs
            style={{
              height: 700,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderTheaterFollowSystem(item)}
          />
        ),
      };
    });
  };

  return (
    <div id="lichchieu" className=" pt-12 pb-12 flex justify-center">
      <Tabs
        style={{
          height: 700,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderInforTheater()}
      />
    </div>
  );
}
