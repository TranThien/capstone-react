import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import BuyTicketPage from "./Page/BuyTicketPage/BuyTicketPage";
import HomePage from "./Page/HomePage/HomePage";
import DetailPage from "./Page/DetailPage/DetailPage";
import LoginPage from "./Page/LoginPage/LoginPage";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import RegisterPage from "./Page/RegisterPage/RegisterPage";
import Layout from "./HOC/Layout";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Layout>
              <HomePage />
            </Layout>
          }
        />
        <Route
          path="/detail/:id"
          element={
            <Layout>
              <DetailPage />
            </Layout>
          }
        />
        <Route
          path="/login"
          element={
            <Layout>
              <LoginPage />
            </Layout>
          }
        />
        <Route
          path="/register"
          element={
            <Layout>
              <RegisterPage />
            </Layout>
          }
        />
        <Route
          path="/buyticket/:id"
          element={
            <Layout>
              <BuyTicketPage />
            </Layout>
          }
        />
        <Route
          path="*"
          element={
            <Layout>
              <NotFoundPage />
            </Layout>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}
export default App;
