import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import { rootReducer } from "./redux/reducer/rootReducer";
import thunk from "redux-thunk";
import persistReducer from "redux-persist/es/persistReducer";
import persistStore from "redux-persist/es/persistStore";
import storage from "redux-persist/lib/storage";
import { PersistGate } from "redux-persist/es/integration/react";

const persistConfig = {
  key: "root",
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const root = ReactDOM.createRoot(document.getElementById("root"));

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  // rootReducer,
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk))
);
const persistore = persistStore(store);
root.render(
  <Provider store={store}>
    <PersistGate persistor={persistore}>
      <App />
    </PersistGate>
  </Provider>
);
reportWebVitals();
