import { MOVIE_LIST } from "../constant/userContant";

const initialState = {
  movieList: [],
};

export const movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case MOVIE_LIST:
      return { ...state, movieList: payload };

    default:
      return state;
  }
};
