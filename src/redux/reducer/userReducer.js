import { userLocalStorage } from "../../service/localStorageService";
import { USER_LOGIN, USER_LOG_OUT } from "../constant/userContant";

const initialState = {
  // user: userLocalStorage.get(),
  user: null,
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      return { ...state, user: payload };
    case USER_LOG_OUT:
      return { ...state, user: null };
    default:
      return state;
  }
};
