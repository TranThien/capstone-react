import { message } from "antd";
import { userLocalStorage } from "../../service/localStorageService";
import { userService } from "../../service/userService";
import { USER_LOGIN, USER_LOG_OUT } from "../constant/userContant";

export const setUserAction = (value) => {
  return {
    type: USER_LOGIN,
    payload: value,
  };
};

export const setUserActionService = (value, navigation) => {
  return (dispatch) => {
    userService
      .postLogin(value)
      .then((res) => {
        // dùng redux thunk
        // hiển thị mess
        message.success("Đăng nhập thành công");
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        userLocalStorage.set(res.data.content);
        navigation();
      })

      .catch((err) => {
        message.error("Đăng nhập thất bại ! . Xin vui lòng thử lại");
      });
  };
};

// export const setUserLogOut = () => {
//   return (dispatch) => {
//     dispatch({
//       type: USER_LOG_OUT,
//     });
//   };
// };

export const setUserLogOut = (value) => {
  return {
    type: USER_LOG_OUT,
    payload: value,
  };
};
